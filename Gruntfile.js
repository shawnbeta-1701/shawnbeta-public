module.exports = function(grunt) {
  'use strict';

  // Force use of Unix newlines
  grunt.util.linefeed = '\n';

  // Project configuration.
  grunt.initConfig({


    mustache_render: {

      random: {
            options: {
                directory: 'src/components/random/'
            },
            files: [{
                data: 'src/components/random/random.json',
                template: 'src/components/random/random.mustache',
                dest: 'src/components/random/build/random.mustache'
            }]
        },

      dev: {
        options: {
          directory: 'src/components'
        },
        files: [{
          data: 'src/input.json',
          template: 'src/base.mustache',
          dest: 'dist/index.html'
        }]
      },
    },

    copy: {
    dev: {
        files: [
            {expand: true, cwd: 'assets/', src: '**/**', dest: 'dist/'},


        ],
    },
},



    less: {
      dev: {
        options: {
          strictMath: true,
          outputSourceFiles: true
        },
        files: {
            'dist/styles/styles.css': 'src/less/styles.less',
            'dist/styles/ie.css': 'ie.less'
        }
      }
    },
  });

  require( 'load-grunt-tasks')(grunt, {
    scope: 'devDependencies'
  });


  grunt.registerTask('dev', [ 'mustache_render:random', 'mustache_render:dev', 'less:dev', 'copy:dev']);
};
