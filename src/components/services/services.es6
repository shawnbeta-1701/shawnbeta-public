/**
 *
 * SERVICE_COLLECTION: This is the data object that holds all the information on my services
 *
 */
const SERVICE_COLLECTION = [
    {
        name: "Drupal 7/8 Themeing",
        code: "older",
        src: {
            url: "https://github.com/shawnbeta/drupal-8-theme-first-rate-2016",
            desc: "Custom Drupal 7 Theme I developed for my parents landscaping web site"
        }
    },
    {
        name: "Drupal 7/8 Views",
    },
    {
        name: "angular.js/Angular",
        code: "modern",
        src: {
            url: "https://bitbucket.org/shawnbeta-1701/media-manager/src/3ca309e649abf61266a4ba31edc550114613758d/apps/media_manager/src/?at=master",
            desc: "download Angular from Bitbucket."
        }
    },
    {
        name: "jQuery, Javascript",
        code: "modern",
        src: {
            url: "https://bitbucket.org/shawnbeta-1701/media-manager/src/3ca309e649abf61266a4ba31edc550114613758d/apps/media_manager/src/?at=master",
            desc: "download javascript from Bitbucket."
        }
    },
    {
        name: "Service Workers",
    },
    {
        name: "LESS, SASS, Bootstrap",
        code: "modern",
        src: {
            url: "https://bitbucket.org/shawnbeta-1701/shawnbeta-public/src/f0c880c3a8960d62d6b64bd54635e6cff3e53dd7/src/less/?at=public",
            desc: "download less code from Bitbucket."
        }
    },
    {
        name: "HTML5 Video/Audio",
        code: "older",
        src: {
            url: "https://github.com/shawnbeta/herocast/tree/master/scripts/modules/player",
            desc: "download HTML 5 Video/Audio code from Github"
        }
    },
    {
        name: "Responsive Design",
        code: "modern",
        src: {
            url: "https://bitbucket.org/shawnbeta-1701/shawnbeta-public/src/f0c880c3a8960d62d6b64bd54635e6cff3e53dd7/src/components/hero/hero.less?at=public&fileviewer=file-view-default",
            desc: "download responsive designed less file from Bitbucket."
        }
    },
    {
        name: "MySQL",
        code: "modern",
        src: {
            url: "https://bitbucket.org/shawnbeta-1701/media-manager/src/7fa7dfb66dacbc2472881d57ae481e495b4916b3/engine/bundles/MediaManagerBundle/Service/MediaCollectionService.php?at=master&fileviewer=file-view-default",
            desc: "download PDO code from Bitbucket."
        }
    },
    {
        name: "Gulp/Grunt",
        code: "modern",
        src: {
            url: "https://bitbucket.org/shawnbeta-1701/media-manager/src/3ca309e649abf61266a4ba31edc550114613758d/engine/Gruntfile.js?at=master&fileviewer=file-view-default",
            desc: "download one of my grunt files code from Bitbucket."
        }
    },
    {
        name: "Ubuntu",
    },
    {
        name: "Demandware Script",
    },
    {
        name: "ISML templates",
    },
    {
        name: "Responsys",
    },
    {
        name: "Silverpop",
    },
];

let serviceWrapper = document.getElementById('services--wrapper');

/**
 *
 * Iterates over the service collection, creating an element for each
 * item and appending them to the container element.
 *
 * @param viewportWidth int
 *
 * todo: optimize this
 */
function buildServiceCollection(viewportWidth) {
    serviceWrapper.innerHTML = '';
    let services = viewportWidth < 767 ? SERVICE_COLLECTION.slice(0, 6) : SERVICE_COLLECTION;
    services.map((service)=>{
        let bgColor = 'bg-red';
        let fontColor = 'font-white';
        let node;
        let newElement = document.createElement('div');
        if(!service.src){
            node = document.createElement('div');
            let text = document.createTextNode(service.name);
            node.appendChild(text);
            newElement.classList.add(fontColor);
            node.classList.add('padding-1');
        }else {
            node = document.createElement('a');
            node.classList.add('block');
            node.classList.add('padding-1');
            let linkText = document.createTextNode(service.name);
            node.appendChild(linkText);
            node.href = service.src.url;
            bgColor = service.code === 'modern' ? 'bg-green' : 'bg-gold';
            fontColor = service.code === 'modern' ? 'font-white' : 'font-black';
            if(service.code === 'modern'){
                bgColor = 'bg-green';
                fontColor = 'font-white';
            }else{
                bgColor = 'bg-gold';
                fontColor = 'font-black';
                newElement.classList.add('font_h-white');
            }
            node.classList.add(fontColor);
            node.classList.add('no-underline');
            newElement.classList.add('trans');
            newElement.classList.add('bgh-blue');
        }
        newElement.appendChild(node);
        newElement.classList.add('services--item');
        newElement.classList.add(bgColor);
        serviceWrapper.appendChild(newElement);
    });

}

export function initializeServices(app) {
    buildServiceCollection(app.viewportWidth);
}