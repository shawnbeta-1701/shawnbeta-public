let footer_date = document.getElementById('footer-date');

/**
 * Generates the current year
 * and places it in site-footer
 */
function setDate(){
    let today = new Date();
    footer_date.innerHTML = today.getFullYear();
}

export function initializeSiteFooter(){
    setDate();
}