import { initializeServices } from "../components/services/services.es6";
import {getViewportWidth, debounce} from './utility/utilities.es6';
import {initializeSiteFooter} from "../components/site-footer/site-footer.es6";
let app = {};

/**
 *
 * Party starter
 */
let initializeApp = debounce(()=>{
    console.log('debounce');
    app.viewportWidth = getViewportWidth();
    initializeServices(app);
    initializeSiteFooter();
}, 250);

window.addEventListener('resize', initializeApp);

initializeApp();