/**
 *
 * Centralized service for handling setting and triggering subscribed callbacks
 *
 *
 */
let mediator = {

    subscriptions: {},

    /**
     *
     * @param subscription
     * @param callback
     */
    setSubscription:(subscription, callback)=>{
        if(!this.subscriptions[subscription]) this.subscriptions = [];
        this.subscriptions[subscription].push(callback);
    },

    /**
     *
     * @param subscription
     */
    notify: (subscription)=>{
        this.subscriptions[subscription].map(callback=>callback());
    }

}