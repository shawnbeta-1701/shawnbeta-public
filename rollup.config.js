export default {
    entry: 'src/js/app.es6',
    dest: 'dist/js/app.js',
    sourceMap: false,
    format: 'iife',
    onwarn: function(warning) {
        if ( warning.code === 'THIS_IS_UNDEFINED' ) { return; }
        console.warn( warning.message );
    },
}